<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(LaratrustSeeder::class);
        $this->call(UsersTableSeeder::class);
        $this->call(HospitalsTableSeeder::class);
        $this->call(PatientsTableSeeder::class);
        $this->call(ProceduresTableSeeder::class);
        $this->call(NotesTableSeeder::class);
        $this->call(BillingsTableSeeder::class);
    }
}
