<?php

use Faker\Generator as Faker;

$factory->define(App\Procedure::class, function (Faker $faker) {
    return [
        //
        'name' => $faker->word,
        'description' => $faker->text($maxNbChars = 200),
        'patient_id' => function () {
            return factory(App\Patient::class)->create()->id;
        }
    ];
});
