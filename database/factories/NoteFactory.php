<?php

use Faker\Generator as Faker;

$factory->define(App\Note::class, function (Faker $faker) {
    return [
        //
        'notes' => $faker->text($maxNbChars = 200),
        'procedure_id' => function () {
            return factory(App\Procedure::class)->create()->id;
        }
    ];
});
