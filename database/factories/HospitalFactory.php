<?php

use Faker\Generator as Faker;

$factory->define(App\Hospital::class, function (Faker $faker) {
    return [
        //
        'name' => $faker->company,
        'town' => $faker->city,
        'county' => $faker->state
    ];
});
