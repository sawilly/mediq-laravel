<?php

use Faker\Generator as Faker;

$factory->define(App\Patient::class, function (Faker $faker) {

    $gender = $faker->randomElement(['male', 'female']);

    return [
        //
        'birth_date' => $faker->date($format = 'Y-m-d', $max = 'now'),
        'photo' => $faker->imageUrl($width = 640, $height = 480),
        'firstname' => $faker->firstName($gender),
        'middlename' => $faker->firstName($gender),
        'lastname' => $faker->lastName,
        'email' => $faker->email,
        'gender' => $gender,
        'patient_no' => $faker->randomNumber($nbDigits = NULL, $strict = false, $min = 10, $max = 10),
        'hospital_id' => function () {
            return factory(App\Hospital::class)->create()->id;
        },
        'user_id' => function () {
            return factory(App\User::class)->create()->id;
        }
    ];
});
