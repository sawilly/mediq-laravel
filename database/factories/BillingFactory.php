<?php

use Faker\Generator as Faker;

$factory->define(App\Billing::class, function (Faker $faker) {

    $billed = $faker->randomElement(['insurance', 'hospital', 'patient']);

    return [
        //
        'procedure_id' => function () {
            return factory(App\Procedure::class)->create()->id;
        },
        'total_cost' => $faker->randomFloat($nbMaxDecimals = NULL, $min = 0, $max = NULL),
        'amount_received' => $faker->randomFloat($nbMaxDecimals = NULL, $min = 0, $max = NULL),
        'balance' => $faker->randomFloat($nbMaxDecimals = NULL, $min = 0, $max = NULL),
        'billed_to' => $billed
    ];
});
