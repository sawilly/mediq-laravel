<?php

use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(App\User::class, function (Faker $faker) {
    $gender = $faker->randomElement(['male', 'female']);
    return [
        'firstname' => $faker->firstName($gender),
        'middlename' => $faker->firstName($gender),
        'lastname' => $faker->lastName,
        'email' => $faker->unique()->safeEmail,
        'national_id' => $faker->randomNumber($nbDigits = NULL, $strict = false),
        'doctor_speciality' => $faker->sentence($nbWords = 5, $variableNbWords = true),
        'gender' => $gender,
        'birth_date' => $faker->date($format = 'Y-m-d', $max = 'now'),
        'board_no' => $faker->randomNumber($nbDigits = 5, $strict = false),
        'personal_number' => $faker->randomNumber($nbDigits = NULL, $strict = false, $min = 10, $max = 10),
        'office_number' => $faker->randomNumber($nbDigits = NULL, $strict = false, $min = 10, $max = 10),
        'password' => '$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm', // secret
        'remember_token' => str_random(10),
    ];
});
