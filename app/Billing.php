<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Billing extends Model
{
    /**
    * Get Procedure For The Note.
    */
    public function procedure()
    {
        return $this->belongsTo('App\Procedure', 'procedure_id');
    }
}
