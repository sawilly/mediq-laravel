<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Hospital extends Model
{
    /**
     * Get Patients Belonging To The Hospital.
     */
    public function patients()
    {
        return $this->hasMany('App\Patient', 'hospital_id');
    }
}
