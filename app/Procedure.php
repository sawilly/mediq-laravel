<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Procedure extends Model
{
    /**
    * Get Notes Belonging To The Procedure.
    */
    public function notes()
    {
        return $this->hasMany('App\Note', 'procedure_id');
    }

    /**
    * Get Billings Belonging To The Procedure.
    */
    public function billings()
    {
        return $this->hasMany('App\Billing', 'procedure_id');
    }

    /**
    * Get Patient Undergoing The Procedure.
    */
    public function patient()
    {
        return $this->belongsTo('App\Patient', 'patient_id');
    }
}
