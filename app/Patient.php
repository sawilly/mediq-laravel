<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Patient extends Model
{
    /**
    * Get Hospital The Patient Belongs To.
    */
    public function hospital()
    {
        return $this->belongsTo('App\Hospital', 'hospital_id');
    }

    /**
    * Get Doctor Treating The Patient.
    */
    public function doctor()
    {
        return $this->belongsTo('App\User', 'user_id');
    }
    
    /**
    * Get Procedures Done For The Patient.
    */
    public function procedures()
    {
        return $this->hasMany('App\Procedure', 'patient_id');
    }
}
