<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Patient;

class PatientsController extends Controller
{
    //Displaying All Patients From DB
    public function index() {

        return Patient::where('user_id', auth()->user()->id)->orderBy('id', 'DESC')->get();

    }
}
