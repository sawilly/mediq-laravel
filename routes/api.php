<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

//Login Api Route
Route::post('/login', ['uses' => 'AuthController@login']);

//Grouping APIs under auth middleware
Route::middleware('auth:api')->group(function() {

    //Retrieving Information About Currently Logged In User
    Route::get('/user', function (Request $request) {
        return $request->user();
    });

    //Returns All Patients Belonging To Logged In User
    Route::get('/patients', 'PatientsController@index');

    //Logs User Out Of System
    Route::post('/logout', 'AuthController@logout');

});